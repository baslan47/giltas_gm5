import { Injectable } from '@angular/core';
import { Product, Color } from '../models/DataLayer';
import { Settings } from '../models/Settings';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

    isLoggedIn = false;
    loginResponse: any;

    VERSION: string = '1.5.0';
    CONNECT: string = '/IntegratorService/Connect';
    DISCONNECT: string = '/IntegratorService/Disconnect/';
    RUNPROC: string = '/IntegratorService/Runproc/';
    POST: string = '/IntegratorService/Post/';
    QMARK: string = '?';
    APPROVE:string= '/ShipmentService/ApproveTransfer'

    Settings: Settings;
    Items: Product[] = [];
    Colors: Color[] = [];

    private base_url: string = "http://panel.giltas.com.tr/";
    //private base_url:string = "http://localhost:8888/giltas_web/";
    public login_url: string = this.base_url + "ws/check_login.php";
    public logout_url: string = this.base_url + "ws/logout.php";

    public isOpenCamera: boolean = false;
    public openOrdersCount:number=0;
    public notAcceptedShipmentCount:number=0;


    constructor(
        private http: HttpClient,
        private storage: Storage
    ) { }

    login(account: any) {
      const options = {
          headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
      };
      return this.http.post(this.login_url, account, options)
          .toPromise()
          .then(response => {                
              return response;
          })
  }
  connect(integrator, userData) {
      this.Settings.Integrator.Url = integrator;
      return new Promise((resolve, reject) => {
          let headers = new HttpHeaders();
          let url: string = this.Settings.Integrator.Url + this.CONNECT + this.QMARK + JSON.stringify(userData);            
          console.log(url);
          return this.http.get(url, { headers: headers })
              .subscribe((res: any) => {
                  this.Settings.Token = res.Token;
                  resolve(res);
              },
                  (err) => {
                      reject(err);
                  });
      });
  }

  disconnect(): Promise<any> {
      let url: any = this.Settings.Integrator.Url + this.DISCONNECT + this.Settings.Token + this.QMARK;
      return this.http.get(url)
          .toPromise()
          .then((response: any) => {
              return response;
          })
          .catch();
  }
}
