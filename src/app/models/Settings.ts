import { Integrator, CompanyParameters } from './Parameters';

export class G3License {
  Modules: string; 
  /**
   * 0   0    0   0   0   0
   * U   M    S   R   K   R
   * 
   * U : Ürün
   * M : Müşteri
   * S : Satış
   * R : Rapor
   * K : Kampanya
   * R : Raf
   *  */ 

  SalesTypes: string; 
  /**
   * 0    0    0    0   0   0
   * TO   P    TA   I   PSH PSS
   * 
   * TO : Toptan Satış
   * P  : Perakende Satış
   * TA : Taksitli Satış
   * I  : Ihracat Satış
   * PSH: Peşin Satış Hemen Teslim
   * PSS: Peşin Satış Sonra Teslim
   *  */ 

}


export class G3Settings {
  ImageUrl: string;
  OrderCount: number;
  Currency: string;

  // New for ionic 4 & App package.
  UseInventoryInProductDetail: boolean = true;  // true : eke, false : ozsanal
  SendReceiptViaMail: boolean = true; // eke
  SendEmailTo: string; // eke
  EnableMultipleView: Boolean; // eke
  ShowTotalInReports: boolean; // ozsanal
  UseCustomPrice: boolean; // eke
  ShowPaymentPlans: boolean; // ozsanal
  ShowTaxPlans: boolean; // eke

  ForCreditCardPlan: boolean; // eke 0, ozsanal 1 (takistli haric)
  UseColorSizeMatrix: boolean;
  SetIsCreditSaleAndIsCompleted : boolean;
  AddOrderDiscount: boolean;
  
  // is completed
  // is credit sale

}


export class Settings {
  Header: string;
  Integrator: Integrator;
  Token: string;
  G3Settings: G3Settings;
  CompanyParameters: CompanyParameters; //V3Settings
  G3License: G3License;
  FetchDate: number;
}