import { Data } from './DataLayer';

export class CompanyParameters {
    SalespersonCode: string;
    SalespersonNameCode: string;
    SalespersonPassword: string;
    SalespersonName: string;  
    SalespersonEmail: string;  
    OfficeCode: string;
    StoreCode: string;
    WarehouseCode: string;
    IsExchange: boolean;
    POSTerminalID: string;
    PaymentCode: string;
    IsGuarantor: boolean;
    IsSuspended: boolean;
    IsProductPriceByGrCode: boolean;
    IsProductPhotos: boolean;
    CreditLimit: number;
    DiscountType: number;
    SalesType: number;
    PaymentTerm: number;
    PriceGroupCode: string;
    DiscountTypeCode: string;
    DiscountReasonCode: string;
    IsPercentage: boolean;  
  }


  export class Exception {
    ExceptionMessage: string;
    StackTrace: string;
    Source: string;
    Data: Data;
    InnerException?: any;
  }
  
  export class Integrator {
    Url: string;
    DatabaseName: string;
    UserGroupCode: string;
    UserName: string;
    Password: string;
  }