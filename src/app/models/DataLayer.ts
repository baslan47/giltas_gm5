export class IntegratorProduct {
    UsedBarcode: string;
    ItemTypeCode: number;
    PriceVI: number;
    Price: number;
    ProductSerialNumber: string;
    Qty1: any ="";
    PaymentPlanCode: string;
    LDisRate1: any;
  }
  
  export class IntegratorLotProduct {
    ItemTypeCode: number;
    LotBarcode: string;
    Qty1: any ="";
  }

  export class Product {
    ItemCode: string;
    ItemDescription: string;
    Price: number;
    Qty1: any ;
    MaxQty: number = 0;
    UsedBarcode: string;
    PriceList: ProductPrice[];
    PaymentPlanCode: string;
    PaymentPlanProduct: ProductPaymentPlan[];
    LDisRate1: any;
    Content: any;  
    RatePrice: number = 0;
    ColorCode: string;
    ItemDim1Code: string;
    BarcodeType: number;
    seriNumber: any = "";
    UseSerialNumber: boolean;
    MesureCode: any = "";
    MesureCodeDesc:any="";
    Barcode: any;    
  }
  
  export class ProductDetails {
    Inventory: number;
    ItemCode: string;
    ItemDescription: string;
    ItemTaxGrCode: any;
    ProductAtt01Desc: string;
    ProductAtt02Desc: string;
    ProductAtt03Desc: string;
    ProductAtt08Desc: string;
    ProductHierarchyLevel01: string;
    ProductHierarchyLevel02: string;
    ProductHierarchyLevel03: string;
    ProductTypeDescription: string;
    RetailSalePrice: number;
    SalePrice: number;
    MesureCode: string;
    MesureCodeDesc:any="";
    UseSerialNumber: boolean;
  }
  
  export class ProductPrice {
    PaymentPlanCode: string;
    Price: number;
    PriceGroupCode: string;
  }
  
  export class ProductPaymentPlan {
    PaymentPlanCode: string;
    PaymentPlanDescription: string;
    PaymentPercentage: number;
    InstallmentCount: number;
  }
  
  export class PaymentPlan {
    PaymentPlanCode: string;
    PaymentPlanDescription: string;
    PaymentPercentage: number;
    InstallmentCount: number;
  }
  
  export class CustomerDebit {
    CurrAccCode: string;
    DovizAlacak: String;
    DovizBakiye: String;
    DovizBorc: String;
    TLAlacak: String;
    TLBakiye: String;
    TLBorc: String;
  }
  
  export class Color {
    ColorCode: string;
    ColorHex: string;
    ColorDescription: string;
  }

  export class Data {
  }
